from typing import Callable
from scipy.optimize import root


def rootfinder(loss_func: Callable, init_vals: dict):
    result = root(loss_func, init_vals, method="lm")
    if not result.success:
        raise RuntimeError("Steady state finder failed. The solution did not converge. "
                           "Try to use another starting point.")
    return result.x
