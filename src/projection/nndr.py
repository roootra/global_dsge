from .basedr import *
from typing import Sequence
import torch
import scipy.stats as stats


class NNDR(BaseDR):
    def __init__(self, model, state_domains, layers, act_func):
        super(NNDR, self).__init__()
        # Variables
        self.model = model
        self.controls = self.model.controls
        self.endo_states = self.model.states_endo
        self.endo = self.controls + self.endo_states
        self.exo_states = self.model.varexo
        self.states = self.endo_states + self.exo_states
        # Dimension
        self.layers = None
        self.act_func = None
        self.points = None
        self.nn = MLPerceptron(n_input=len(self.states), n_output=len(self.endo), layers=None, act_func=None)

    def generate_points(self, n_per_state: int = 3, distr: str = None, params: list = None,
                        deterministic: bool = False):
        if distr is not None and distr not in ["trunc_norm", "uniform"]:
            raise ValueError("Wrong distribution specified. Use 'trunc_norm', 'uniform' or None.")
        if params is None:
            params = [0, 0.1]  # loc, scale
        if deterministic:
            F = torch.linspace(0, 1, n_per_state)
            if distr == "trunc_norm":
                space = torch.tensor(stats.truncnorm.ppf(F, -1, 1, params[0], params[1]))
            else:
                space = torch.linspace(-1, 1, n_per_state)
            return torch.cartesian_prod(*[space for _ in range(len(self.states))])
        else:
            if distr == "trunc_norm":
                space = torch.tensor(stats.truncnorm.rvs(-1, 1, params[0], params[1], n_per_state), dtype=torch.float32)
                return torch.cartesian_prod(*[space for _ in range(len(self.states))])
            else:
                space = torch.tensor(stats.uniform.rvs(-1, 1, n_per_state), dtype=torch.float32)
                return torch.cartesian_prod(*[space for _ in range(len(self.states))])

    def eval(self, x):
        return self.nn.forward(x)


class MLPerceptron(torch.nn.Module):
    def __init__(self, n_input: int, n_output: int, layers, act_func):
        super(MLPerceptron, self).__init__()
        self.linear_stack = torch.nn.Sequential(
            torch.nn.Linear(n_input, 512),
            torch.nn.ReLU(),
            torch.nn.Linear(512, n_output)
        )
        #torch.nn.MultiLayer([])

    def forward(self, x):
        return self.linear_stack(x)



def nn_init_weights(nn_layer):
    if isinstance(nn_layer, torch.nn.Linear):
        torch.nn.init.uniform_(nn_layer.weight.data, 0.0, 1.0)
        torch.nn.init.uniform_(nn_layer.bias.data, 0.0, 1.0)