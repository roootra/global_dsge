from abc import ABC, abstractmethod


class BaseDR(ABC):
    @abstractmethod
    def __init__(self):
        pass

    @abstractmethod
    def eval(self, x):
        pass
