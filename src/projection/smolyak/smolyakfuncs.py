import torch
import numpy
from itertools import chain, product
from torch import profiler


def m_i(i):
    res = 2**(i-1) + 1
    if isinstance(res, (int, float, numpy.int_, numpy.float_)):
        if res == 2:
            return 1
        else:
            return res
    else:
        res[res == 2] = 1
    return res


def constrained_partitions(n, k, min_elem, max_elem):
    #https://stackoverflow.com/questions/58915599/generate-restricted-weak-integer-compositions-or-partitions-of-an-integer-n-in
    domain = range(max_elem, min_elem - 1, -1)

    def algo(n, k, t):
        if k == 0:
            if n == 0:
                yield t
        elif k == 1:
            if n in domain:
                yield t + (n,)
        elif min_elem * k <= n <= max_elem * k:
            for v in domain:
                yield from algo(n - v, k - 1, t + (v,))
    return algo(n, k, ())


def calculate_orders(q, n):
    i_mod = range(max(n, q-n+1), q + 1)
    orders = []
    for i in i_mod:  #there are (q - 1, n - 1) possible combinations
        orders.append([*constrained_partitions(i, n, 1, i-n+1)])
    i_orders = torch.tensor((*chain(*orders),))
    m_orders = m_i(i_orders)
    indices = set()
    for m_order in m_orders:
        current_orders = product(*[range(m_ord) for m_ord in m_order])
        indices.update([*current_orders])
    return torch.tensor(sorted(indices))


def eval_cheb_prod(x, q, n, orders):
    #max_order = m_i(q-n) + 1  # TODO: why this doesn't work???
    max_order = torch.max(orders)
    res = torch.ones((max_order + 1, x.shape[0]))
    res[1, :] = x
    for i in torch.arange(2, max_order + 1):
        res[i, :] = 2*x*res[i - 1, :] - res[i - 2, :]
    row_enum = torch.arange(orders.shape[1])
    return torch.tensor([torch.prod(res[order, row_enum]) for order in orders])


def eval_smolyak_dr(params, x, q, n, orders):
    cheb_prod = eval_cheb_prod(x, q, n, orders)
    return torch.matmul(params.t(), cheb_prod)


def transform_variables(x, lb, ub, inv=False):
    """
    Transform values in a min/max way to [-1, 1] domain: R^n -> [-1, 1]^n.

    :param x: int, float, array with values of original variable.
    :param lb: int, float, array with lower bounds.
    :param ub: int, float, array with upper bounds.
    :param inv: bool, return inverse transformation ([-1, 1] -> R^n).
    :return: transformed values of the same type as inputs.
    """
    if inv:
        return (x/2 + 0.5) * (ub-lb) + lb
    else:
        return torch.clamp(2*(x - lb)/(ub - lb) - 1, -1, 1)


def generate_clenshaw_points(q, n, eps=1e-16):
    def gen_node(i):
        m = m_i(i)
        if m == 1:
            return numpy.array([0])
        else:
            m_range = numpy.arange(1, m + 1)
            return -numpy.cos(numpy.pi * (m_range - 1) / (m - 1))
    i_mod = range(q-n+1, q + 1)
    permut = []
    for i in i_mod:
        permut.append([*constrained_partitions(i, n, 1, i - n + 1)])
    i_permut = numpy.array((*chain(*permut),))
    nodes = []
    for i_perm in i_permut:
        current_nodes = product(*[gen_node(i) for i in i_perm])
        nodes.append([*current_nodes])
    nodes = numpy.array([*chain(*nodes)])
    nodes[numpy.abs(nodes) < eps] = 0
    return torch.from_numpy(numpy.unique(nodes, axis=0))


if __name__ == "__main__":
    print(generate_clenshaw_points(3, 2))
    breakpoint()
    test = calculate_orders(4, 2)
    print(test)
    with profiler.profile(activities=[profiler.ProfilerActivity.CPU], record_shapes=True, profile_memory=True) as prof:
        x = torch.tensor([0, 0, 0], dtype=torch.float)
        orders = calculate_orders(4, 3)
        print("Sum terms are: ", eval_cheb_prod(x, 4, 3, orders))
        params = torch.tensor([[1] * 7] * 3, dtype=torch.float, requires_grad=True)
        print("Parameters are:\n", params)
        eval = eval_smolyak_dr(params, x, 4, 3, orders)
        print("Evaluated multivariate DR values are: ", eval)
        eval.backward(torch.ones(params.shape[0]), retain_graph=True)
        print("Diag-Jacobian matrix of multivariate DR is:\n", params.grad)
        params.grad = None
        eval_next = eval_smolyak_dr(params, eval, 4, 3, orders)
        print("Next iteration of multivariate DR is:", eval_next)
        eval_next.backward(torch.ones(params.shape[0]), retain_graph=True)
        print("Diag-Jacobian matrix of the next iteration of multivariate DR is:\n", params.grad)
    print(prof.key_averages(group_by_input_shape=True).table(sort_by="cpu_time_total", row_limit=10))
    print(prof.key_averages().table(sort_by="cpu_memory_usage", row_limit=10))
    #prof.export_chrome_trace("trace.json")