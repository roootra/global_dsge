import torch
import numpy
from src.projection.basedr import BaseDR
from src.projection.smolyak.smolyakfuncs import *
from typing import Sequence, Any


class SmolyakDR(BaseDR):
    def __init__(self, q: int, controls: list, endo_states: list, exo_states: list, init_params: Any = None):
        super(SmolyakDR, self).__init__()
        self.controls = controls
        self.endo_states = endo_states
        self.exo_states = exo_states
        self.q = q
        self.n = len(endo_states) + len(exo_states)
        self.c = len(controls) + len(endo_states)
        self.orders = calculate_orders(self.q, self.n)
        self.params = self._prep_params(init_params)

    def _prep_params(self, init_params: Any) -> torch.Tensor:
        if init_params is None:
            params = torch.zeros((self.orders.shape[0], self.c))
            params[0, :] = 1.0
            return params
        elif isinstance(init_params, (torch.Tensor, numpy.ndarray)):
            if init_params.shape != torch.Size([self.orders.shape[0], self.c]):
                raise RuntimeError(f"Shape of initial parameters is wrong: "
                                   f"got {init_params.shape}, needed {self.orders.shape}.")
            return torch.tensor(init_params)
        elif isinstance(init_params, Sequence):
            if len(init_params) != self.orders.shape[0] * self.c:
                raise RuntimeError(f"Length of initial parameters is wrong: "
                                   f"got {len(init_params)}, needed {self.orders.shape[0] * self.c}.")
            return torch.tensor(init_params).reshape(-1, self.c)
        elif isinstance(init_params, dict):
            if "type" not in init_params.keys():
                raise RuntimeError("Unsupported dict structure: 'type' key is required.")
            if init_params["type"] == "random":
                if "domain" not in init_params.keys():
                    raise RuntimeError("Random parameter initialization is invoked, but no domain is specified.")
                return torch.rand((self.orders.shape[0], self.c)) * \
                       (init_params["domain"][1] - init_params["domain"][0]) + init_params["domain"][0]
            if init_params["type"] == "const":
                if "values" not in init_params.keys():
                    raise RuntimeError("Constant parameter initialization is invoked, but no values are specified.")
                params = torch.torch.zeros((self.orders.shape[0], self.c))
                for pos, endo_var in enumerate(self.controls + self.endo_states):
                    params[0, pos] = init_params["values"][endo_var]
                return params
            if init_params["type"] == "explicit":
                params = torch.zeros((self.orders.shape[0], self.c))
                ord_lst = self.orders.tolist()
                for pos, endo_var in enumerate(self.controls + self.endo_states):
                    for index, val in init_params[endo_var].items():
                        current_index_position = ord_lst.index(list(index))
                        params[current_index_position, pos] = val
                return params

    def get_params(self, return_dict: bool = False) -> Any:
        if return_dict:
            params_dict = {var: dict() for var in self.controls + self.endo_states}
            ord_lst = self.orders.tolist()
            for col, var in enumerate(params_dict.keys()):
                for row, index in enumerate(ord_lst):
                    params_dict[var][tuple(index)] = float(self.params[row, col])
            params_dict["type"] = "explicit"
            return params_dict
        else:
            return self.params

    def set_params(self, new_params: Any) -> None:
        self.params = self._prep_params(new_params)

    def eval_with_autograd(self, x: torch.Tensor, params: Any) -> torch.Tensor:
        return eval_smolyak_dr(params, x, self.q, self.n, self.orders)

    def eval(self, x: torch.Tensor) -> torch.Tensor:
        return eval_smolyak_dr(self.params, x, self.q, self.n, self.orders)


if __name__ == "__main__":
    controls = ["c", "l"]
    endo_states = ["k", "A"]
    exo_states = ["eps"]
    random_params_dict = {"type": "random", "domain": [0.0, 1.0]}
    const_params_dict = {"type": "const", "values": {"c": 1, "l": 2, "k": 3, "A": 4}}
    x = torch.Tensor([1.0, 2.0, 3.0])
    test_dr = SmolyakDR(4, controls, endo_states, exo_states, const_params_dict)
    print("Const initialization")
    print(test_dr.params)
    print("Random initialization")
    test_dr_4 = SmolyakDR(4, controls, endo_states, exo_states, random_params_dict)
    print(test_dr_4.params)
    test_dr_q4 = test_dr_4.get_params(True)
    print("Exported random params:")
    print(test_dr_q4)
    test_dr = SmolyakDR(5, controls, endo_states, exo_states, test_dr_q4)
    print("Params for q=5 from q=4:")
    print(test_dr.params)
    print(test_dr.get_params(True))
    print(test_dr.eval(x))

    print("Testing autograd")
    test_params = torch.tensor(test_dr_4.get_params(False), requires_grad=True)
    test_dr_autograd = SmolyakDR(4, controls, endo_states, exo_states, test_params)
    test_dr_result = test_dr_autograd.eval_with_autograd(x, test_params)
    #test_dr_result.backward(torch.ones(4))
    print(test_params.grad)
    print("Tesing Pytorch Jacobian:")
    print(torch.autograd.functional.jacobian(lambda params: test_dr_autograd.eval_with_autograd(x, params),
                                             test_params))
