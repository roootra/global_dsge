import torch
import numpy
from torch.autograd.functional import jacobian as torch_jac
from src.solvers.basesolver import BaseSolver
from projection.smolyak.smolyakfuncs import transform_variables, generate_clenshaw_points
from projection.smolyakdr import SmolyakDR
from typing import Optional, Any, Union
from functools import partial
import re
from scipy import optimize


class SmolyakSolver(BaseSolver):
    def __init__(self, model: Any, state_domains: Optional[dict] = None, states: Optional[list] = None):
        super(SmolyakSolver, self).__init__(model=model, state_domains=state_domains, states=states)
        # Variables
        self.model = model
        self.controls = self.model.controls
        self.endo_states = self.model.states_endo
        self.endo = self.model.endo
        self.shocks = self.model.shocks
        # Dimension
        self.q = None
        self.min_q = len(self.endo_states + self.shocks) + 1
        self.multistep = None
        # Solution
        self.state_domains_tensor = self._format_state_domains()
        self.nodes = None
        self.dr = None
        self.formatted_eqns = self._prepare_equations(self.model.dynamic_eqns)
        self.loss_func, self.loss_code = self._generate_loss_func(self.formatted_eqns)
        print(self.loss_code)

    def _format_state_domains(self):
        state_dom = torch.zeros((len(self.endo_states + self.shocks), 2))
        for i, var in enumerate(self.endo_states + self.shocks):
            state_dom[i, :] = torch.tensor(self.state_domains[var])
        return state_dom

    def _prepare_equations(self, raw_eqns):
        formatted_eqns = list()
        for raw_eqn in raw_eqns:
            for i, parameter in enumerate(self.model.param_values.keys()):  # fill parameters
                raw_eqn = re.sub(rf"(\W|^)({parameter})(\W|$)", rf"\g<1>___PARAMETER___[{i}]\g<3>", raw_eqn)
            for i, var in enumerate(self.endo_states):
                raw_eqn = re.sub(rf"{var}(\(-1\))", rf"___STATE_L___[{i}]", raw_eqn)
                raw_eqn = re.sub(rf"(\b){var}(?!\B|[\(])", rf"___STATE___[{i}]", raw_eqn)
                raw_eqn = re.sub(rf"{var}(\([\+]?1\))", rf"___STATE_F___[{i}]", raw_eqn)
            for i, var in enumerate(self.controls):
                raw_eqn = re.sub(rf"(\b){var}(?!\B|[\(])", rf"___CONTROL___[{i}]", raw_eqn)
                raw_eqn = re.sub(rf"{var}(\([\+]?1\))", rf"___CONTROL_F___[{i}]", raw_eqn)
            for i, exo in enumerate(self.shocks):
                raw_eqn = re.sub(rf"(\b){exo}(?!\B|[\(])", rf"___SHOCK___[{i}]", raw_eqn)
            formatted_eqns.append(raw_eqn)
        return formatted_eqns

    def _generate_loss_func(self, formatted_eqns: list):
        eqns_ndarray = \
            [f"result[{i}] = " + formatted_eqns[i] for i in range(0, len(formatted_eqns))]
        nl="\n    "
        func_str = f"""import torch
from torch import tensor
from torch import log, exp, min, max, sin, cos, tan, asin, acos, atan, pi
def loss(___STATE_L___, ___STATE___, ___STATE_F___, ___CONTROL___, ___CONTROL_F___, ___SHOCK___, ___PARAMETER___):
    result = torch.zeros(({len(eqns_ndarray)},))
    {nl.join(eqns_ndarray)}
    return result
                """
        func_holder = {}
        exec(func_str, func_holder)
        return func_holder["loss"], func_str

    def _calculate_loss_at_clenshaw(self, params: torch.Tensor, norm: Optional[int] = None):
        params = params.reshape(len(params.ravel()) // len(self.endo), len(self.endo))
        result = list()
        ___PARAMETER___ = torch.tensor(list(self.model.param_values.values()))
        shocks_shift = len(self.endo_states)
        for i, node in enumerate(self.nodes):
            # lags
            states_predetermined = transform_variables(node, self.state_domains_tensor[:, 0],
                                                        self.state_domains_tensor[:, 1], inv=True)
            states_lags = states_predetermined[:shocks_shift]
            shocks_current = states_predetermined[shocks_shift:]
            # current
            endo_current = self.dr.eval_with_autograd(x=node, params=params)
            states_endo_current = endo_current[len(self.controls):]
            controls_current = endo_current[:len(self.controls)]
            shocks_next = torch.zeros(len(self.shocks))
            states_current = torch.concat((states_endo_current, shocks_next))
            # leads
            states_current_transf = transform_variables(states_current, self.state_domains_tensor[:, 0],
                                                        self.state_domains_tensor[:, 1])
            endo_new = self.dr.eval_with_autograd(x=states_current_transf, params=params)
            states_new = endo_new[len(self.controls):]
            controls_new = endo_new[:len(self.controls)]
            ___STATE_L___ = states_lags
            ___STATE___ = states_current
            ___STATE_F___ = states_new
            ___CONTROL___ = controls_current
            ___CONTROL_F___ = controls_new
            ___SHOCK___ = shocks_current
            loss_val = self.loss_func(___STATE_L___, ___STATE___, ___STATE_F___,
                                      ___CONTROL___, ___CONTROL_F___,
                                      ___SHOCK___, ___PARAMETER___)
            result += loss_val
        if norm == 1:
            result = torch.abs(torch.stack(result)).sum()
        elif norm == 2:
            result = torch.square(torch.stack(result)).sum()
        else:
            result = torch.stack(result)
        return result

    def _solv_func(self, params, norm: Optional[int] = None):
        ev = self._calculate_loss_at_clenshaw(torch.tensor(params, dtype=torch.float32), norm)
        return ev.numpy()
        #return torch.stack(ev).numpy()

    def _jac_func(self, params, norm: Optional[int] = None):
        params_tensor = torch.tensor(params, dtype=torch.float32)
        params_tensor.requires_grad = True
        if norm is not None:
            interm_func = partial(self._calculate_loss_at_clenshaw, norm=norm)
            jac = torch_jac(interm_func, params_tensor, create_graph=False)
        else:
            jac = torch_jac(self._calculate_loss_at_clenshaw, params_tensor, create_graph=False)
        return jac.numpy()
        #return torch.stack(jac).numpy()

    def solve(self, d_q: int, multistep: bool = False, opt_method_kwargs: dict = None,
              solver_type: Optional[str] = 'root', norm: Optional[int] = 2,
              init_params: Union[None, float, list, dict] = None,
              inf_val: float = 1e6, verbose: bool = False):
        self.q = self.n_states + d_q
        if multistep:
            q_steps = range(self.n_states + 1, self.q + 1)
            if len(q_steps) > 1:
                print(f"Multistep solution scheme is invoked. There are {len(q_steps)} q-values, "
                      f"from {q_steps[0]} to {q_steps[-1]}.")
            else:
                print(f"Δ(q) = 1, multistep scheme is disabled.")
        else:
            q_steps = [self.q]
        for q in q_steps:
            self.nodes = generate_clenshaw_points(q, self.n_states)
            print(f"Solving for q = {q}. There are {len(self.nodes)} parameters.")
            if opt_method_kwargs is None:
                opt_method_kwargs = dict()
            self.dr = SmolyakDR(q, self.controls, self.endo_states, self.shocks, init_params)
            init_params_scipy = self.dr.get_params().clone().detach()
            if solver_type == 'min':
                solv_func = partial(self._solv_func, norm=norm)
                jac_func = partial(self._jac_func, norm=norm)
                opt_res = optimize.minimize(fun=solv_func, jac=jac_func, x0=init_params_scipy, **opt_method_kwargs)
            else:
                opt_res = optimize.root(fun=self._solv_func, jac=self._jac_func, x0=init_params_scipy, **opt_method_kwargs)
            self.dr.set_params(opt_res.x.tolist())
            init_params = self.dr.get_params(return_dict=True)
            if verbose:
                print(opt_res)
            if not opt_res["success"]:
                raise RuntimeError(f"Falied to approximate the model. Try again with other initial values.\n"
                                   f"Message: {opt_res['message']}")
            else:
                print(f"Approximation succeeded. Message: {opt_res['message']}")
        return opt_res.x

    def stoch_simul(self, initial_state: dict, shocks: dict, periods: int, ortho: bool):
        # TODO: do smth with correlated shocks
        simulations = dict()
        states = self.endo_states + self.shocks
        for shock, shock_val in shocks.items():
            for key, val in initial_state.items():
                initial_state[key] = torch.tensor(val, dtype=torch.float32)
            current_shocks = dict((shock, torch.tensor(0.0)) for shock in self.model.shocks)
            current_shocks[shock] = shock_val
            initial_state = {**initial_state, **current_shocks}
            simulations[shock] = list()
            simulations[shock].append(initial_state)
            last_states = torch.zeros((len(self.endo_states + self.shocks)))
            for t in range(1, periods + 1):
                for i in range(last_states.shape[0]):
                    last_states[i] = simulations[shock][t - 1][states[i]]
                last_states = transform_variables(last_states,
                                                  self.state_domains_tensor[:, 0], self.state_domains_tensor[:, 1])
                current_vals = numpy.around(self.dr.eval(last_states), 5)
                current_vals = {var: current_vals[i] for i, var in enumerate(self.endo)}
                for exo in self.model.shocks:
                    current_vals[exo] = torch.tensor(0.0)
                simulations[shock].append(current_vals)
        return simulations