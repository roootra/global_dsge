import torch
import numpy
import re
from typing import Optional, Any, Union
import matplotlib
from matplotlib import pyplot as plt
from torch.autograd.functional import jacobian as torch_jac
from src.solvers.basesolver import BaseSolver
from projection.smolyak.smolyakfuncs import transform_variables
from projection.nndr import NNDR, nn_init_weights


class NNSolver(BaseSolver):
    def __init__(self, model: Any, state_domains: Optional[dict] = None, states: Optional[list] = None):
        super(NNSolver, self).__init__(model=model, state_domains=state_domains, states=states)
        # Variables
        self.model = model
        self.controls = self.model.controls
        self.endo_states = self.model.states_endo
        self.endo = self.controls + self.endo_states
        self.exo_states = self.model.varexo
        # Dimension
        self.layers = None
        self.act_func = None
        self.points = None
        # Solution
        self.state_domains_tensor = self._format_state_domains()
        self.dr = None
        self.formatted_eqns = self._prepare_equations(self.model.dynamic_eqns)
        self.loss_func, self.loss_code = self._generate_loss_func(self.formatted_eqns)
        matplotlib.use('WXAgg')
        print(self.loss_code)

    def _format_state_domains(self):
        state_dom = torch.zeros((len(self.endo_states + self.exo_states), 2))
        for i, var in enumerate(self.endo_states + self.exo_states):
            state_dom[i, :] = torch.tensor(self.state_domains[var])
        return state_dom

    def _prepare_equations(self, raw_eqns):
        formatted_eqns = list()
        for raw_eqn in raw_eqns:
            for i, parameter in enumerate(self.model.param_values.keys()):  # fill parameters
                raw_eqn = re.sub(rf"(\W|^)({parameter})(\W|$)", rf"\g<1>___PARAMETER___[{i}]\g<3>", raw_eqn)
            for i, var in enumerate(self.endo_states):
                raw_eqn = re.sub(rf"{var}(\(-1\))", rf"___PREDETERMINED___[...,{i}]", raw_eqn)
            for i, var in enumerate(self.endo):
                raw_eqn = re.sub(rf"(\b){var}(?!\B|[\(])", rf"___CURRENT___[...,{i}]", raw_eqn)
                raw_eqn = re.sub(rf"{var}(\([\+]?1\))", rf"___NEXT___[...,{i}]", raw_eqn)
            for i, exo in enumerate(self.exo_states):
                raw_eqn = re.sub(rf"(\b){exo}(?!\B|[\(])", rf"___PREDETERMINED___[...,{len(self.endo_states) + i}]",
                                 raw_eqn)
                raw_eqn = re.sub(rf"{exo}(\(\+1\))", rf"___CURRENT___[...,{len(self.endo_states) + i}]", raw_eqn)
            formatted_eqns.append(raw_eqn)
        return formatted_eqns

    def _generate_loss_func(self, formatted_eqns: list):
        eqns_ndarray = \
            [f"result[..., {i}] = " + formatted_eqns[i] for i in range(0, len(formatted_eqns))]
        nl="\n    "
        func_str = f"""import torch
from torch import tensor
from torch import log, exp, min, max, sin, cos, tan, asin, acos, atan, pi
def loss(___PREDETERMINED___, ___CURRENT___, ___NEXT___, ___PARAMETER___, batch_size):
    result = torch.zeros((batch_size, {len(eqns_ndarray)},))
    {nl.join(eqns_ndarray)}
    return result
                """
        func_holder = {}
        exec(func_str, func_holder)
        return func_holder["loss"], func_str

    def _loss(self, state_points: torch.Tensor, ___PARAMETER___: torch.Tensor):
        states_old = state_points
        endo_current = self.dr.eval(x=states_old)
        exos_next = torch.zeros((state_points.shape[0], len(self.exo_states)))
        states_current = torch.hstack((endo_current[..., len(self.controls):], exos_next))
        states_current_transf = transform_variables(states_current,
                                                    self.state_domains_tensor[:, 0], self.state_domains_tensor[:, 1])
        endo_new = self.dr.eval(x=states_current_transf)
        # format variables to use in local variables set with numexpr
        ___PREDETERMINED___ = transform_variables(states_old, self.state_domains_tensor[:, 0],
                                                  self.state_domains_tensor[:, 1], True)
        ___CURRENT___ = endo_current
        ___NEXT___ = endo_new
        eval = self.loss_func(___PREDETERMINED___, ___CURRENT___, ___NEXT___, ___PARAMETER___, state_points.shape[0])
        return torch.mean(torch.square(eval))
        #return eval

    def _solv_func(self, params):
        ev = self._loss(torch.tensor(params, dtype=torch.float32))
        return torch.stack(ev).numpy()

    def _jac_func(self, params):
        params_tensor = torch.tensor(params, dtype=torch.float32)
        params_tensor.requires_grad = True
        jac = torch_jac(self._loss, params_tensor, create_graph=False)
        return torch.stack(jac).numpy()

    def solve(self, points_per_state: int = 5, include_ss: bool = True, distr: str = None, distr_params: list = None,
              layers: list = (256, 128, 64), batch: int = 200, epochs: int = 50, shuffle: bool = True,
              act_func: Any = torch.nn.GELU, optimizer: Any = torch.optim.Adam, lr: float = 0.003,
              opt_method_kwargs: dict = None, init_params: Any = None, verbose: bool = False):
        self.layers = layers
        self.act_func = act_func
        self.dr = NNDR(model=self.model, state_domains=self.state_domains, layers=layers, act_func=act_func)
        self.dr.nn.apply(nn_init_weights)
        optim = optimizer(self.dr.nn.parameters(), lr=lr)
        lr_scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optim, patience=10)
        eq_parameters = torch.tensor(list(self.model.param_values.values()))
        epoch_loss_mean = torch.zeros((epochs,), requires_grad=False)
        if include_ss:
            ss = torch.zeros((1, len(self.model.states_endo + self.model.varexo)))
        for epoch in range(epochs):
            random_points = self.dr.generate_points(points_per_state, distr, distr_params)
            dataloader = torch.utils.data.DataLoader(random_points, batch_size=batch, shuffle=shuffle)
            for i_batch, batch_inst in enumerate(dataloader):
                if include_ss:
                    batch_inst = torch.vstack((batch_inst, ss))
                current_loss = self._loss(batch_inst, eq_parameters)
                optim.zero_grad()
                current_loss.backward()
                optim.step()
                with torch.no_grad():
                    epoch_loss_mean[epoch] += current_loss / batch
                    if verbose:
                        with plt.ion():
                            pass
            print("\r", f"Epoch: {epoch}, RMSE: {torch.sqrt(epoch_loss_mean[epoch])}.", end="")
        return self.dr.nn.parameters()

    def stoch_simul(self, initial_state: dict, shocks: dict, periods: int, ortho: bool):
        # TODO: do smth with correlated shocks
        simulations = dict()
        states = self.endo_states + self.exo_states
        for shock, shock_val in shocks.items():
            current_shocks = dict((varexo, 0) for varexo in self.model.varexo)
            current_shocks[shock] = shock_val
            print(initial_state)
            initial_state = {**initial_state, **current_shocks}
            simulations[shock] = list()
            simulations[shock].append(initial_state)
            last_states = torch.zeros((len(self.endo_states + self.exo_states)))
            for t in range(1, periods+1):
                for i in range(last_states.shape[0]):
                    last_states[i] = simulations[shock][t - 1][states[i]]
                last_states = transform_variables(last_states,
                                                  self.state_domains_tensor[:, 0], self.state_domains_tensor[:, 1])
                current_vals = self.dr.eval(last_states)
                current_vals = {var: current_vals[i] for i, var in enumerate(self.endo)}
                for exo in self.model.varexo:
                    current_vals[exo] = 0
                simulations[shock].append(current_vals)
        return simulations
