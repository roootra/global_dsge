# natives
import re
from typing import Any, Optional
# third-parties
import scipy
# internal
from parsers import BaseParser


class SmolyakSolver(object):
    def __init__(self, model: BaseParser, state_domains: Optional[dict] = None):
        # general
        self.model = model
        self.state_domains = state_domains
        # solver-specific
        self.min_q = len(self.model.states_endo + self.model.shocks) + 1
        self.eqs = self._format_eqs()


    def _format_eqs(self):
        formatted_eqns = list()
        for raw_eqn in self.model.dynamic_eqns:
            for i, parameter in enumerate(self.model.param_values.keys()):  # fill parameters
                raw_eqn = re.sub(rf"(\W|^)({parameter})(\W|$)", rf"\g<1>___PARAMETER___[{i}]\g<3>", raw_eqn)
            for i, var in enumerate(self.endo_states):
                raw_eqn = re.sub(rf"{var}(\(-1\))", rf"___STATE_L___[{i}]", raw_eqn)
                raw_eqn = re.sub(rf"(\b){var}(?!\B|[\(])", rf"___STATE___[{i}]", raw_eqn)
                raw_eqn = re.sub(rf"{var}(\([\+]?1\))", rf"___STATE_F___[{i}]", raw_eqn)
            for i, var in enumerate(self.controls):
                raw_eqn = re.sub(rf"(\b){var}(?!\B|[\(])", rf"___CONTROL___[{i}]", raw_eqn)
                raw_eqn = re.sub(rf"{var}(\([\+]?1\))", rf"___CONTROL_F___[{i}]", raw_eqn)
            for i, exo in enumerate(self.shocks):
                raw_eqn = re.sub(rf"(\b){exo}(?!\B|[\(])", rf"___SHOCK___[{i}]", raw_eqn)
            formatted_eqns.append(raw_eqn)
        return formatted_eqns

