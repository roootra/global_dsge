import re
import json
import os
import numpy
from numexpr import evaluate
from baseparser import BaseParser


class NativeJSONParser(BaseParser):
    def __init__(self, path: str):
        super(NativeJSONParser, self).__init__()
        with open(path, "r") as file:
            self.json = json.load(file)
        print(self.json)
        self.endo = self._parse_vars("endo")
        self.exo = self._parse_vars("exo")
        self.shocks = self._parse_vars("shocks")
        self.param_values = self._parse_vars("params")
        self.params = list(self.param_values.keys())
        self.dynamic_eqns = self._parse_eqns("model", False)
        self.static_eqns = self._parse_eqns("model", True)
        self.dynamic_processes = self._parse_eqns("processes", False)
        self.static_processes = self._parse_eqns("processes", True)
        self.ss_init_vals = self._eval_ss_init_vals()
        self.leads_lags = self._leads_lags()
        self.states_endo, self.controls = self._det_var_type()
        self.exo_covar_mat = self._parse_covar_mat()
        print(self.exo_covar_mat)

    def _parse_vars(self, type: str):
        if type not in self.json.keys():
            raise RuntimeError(f"Model definition block '{type}' is not filled, stopping...")
        return self.json[type]

    def _parse_eqns(self, type: str, static: bool):
        if type not in self.json.keys():
            raise RuntimeError(f"Model definition block '{type}' is not filled, stopping...")
        str_eqns = self.json[type]
        eqns = list()
        for str_eqn in str_eqns:
            lhs_rhs = str_eqn.split("=")
            eqns.append(lhs_rhs[0] + "- (" + lhs_rhs[1] + ")")
        if static:
            static_eqns = list()
            or_clause = "".join(var + "|" for var in self.shocks)[:-1]
            for eq in eqns:
                eq = re.sub(r"([a-zA-Z0-9]+)\(\+*-*\d+\)", r"\1", eq)
                eq = re.sub(rf"(\W|^)({or_clause})(\W|$)", r"\g<1>0\g<3>", eq)
                static_eqns.append(eq)
            eqns = static_eqns
        return eqns

    def _eval_ss_init_vals(self):
        init_val_expressions = self.json["steady_init"]
        init_vals = dict()
        for var, expression in init_val_expressions.items():
            if isinstance(expression, (int, float)):
                init_vals[var] = expression
            else:
                init_vals[var] = float(evaluate(expression, {**self.param_values, **init_vals}))
        return init_vals

    def _leads_lags(self):
        leads_lags = dict()
        combined_eqns = ";".join([*self.dynamic_eqns]).replace(" ", "")
        combined_processes = ";".join([*self.dynamic_processes]).replace(" ", "")
        combined = combined_eqns + ";" + combined_processes
        for var in [*self.endo, *self.exo, *self.shocks]:
            leads_lags[var] = list(set(
                re.findall(f"{var}\(([\+\-0-9+]+)\)", combined)
            ))
            if len(set(re.findall(f"{var}(?!\(|[a-z])", combined))) > 0:
                leads_lags[var] = leads_lags[var] + [0]
            leads_lags[var] = list(map(int, leads_lags[var]))
        return leads_lags

    def _det_var_type(self):
        states = list()
        controls = list()
        for variable, lags in self.leads_lags.items():
            if variable in self.shocks:
                pass
            elif -1 in lags and 0 in lags:  # purely predetermined variables
                states.append(variable)
            elif -1 in lags and 0 in lags and 1 in lags:  # mixed variables
                states.append(variable)
            else:
                controls.append(variable)
        return states, controls

    def _parse_covar_mat(self):
        covars = self.json["covariance"]
        covar_mat = numpy.zeros((len(self.shocks),) * 2)
        for var, val in covars.items():
            if "," in var:  # then, it's corr
                var1, var2 = var.split(",")
                var1_ind, var2_ind = self.shocks.index(var1), self.shocks.index(var2)
                if isinstance(val, (float, int)):
                    covar_mat[var1_ind, var2_ind] = val**2
                    covar_mat[var2_ind, var1_ind] = val**2
                else:
                    covar_mat[var1_ind, var2_ind] = float(evaluate(val, self.param_values))
                    covar_mat[var2_ind, var1_ind] = covar_mat[var1_ind, var2_ind]
            else:  # then, it's std. dev.
                var_ind = self.shocks.index(var)
                if isinstance(val, (float, int)):
                    covar_mat[var_ind, var_ind] = val**2
                else:
                    covar_mat[var_ind, var_ind] = float(evaluate(val, self.param_values))
        return covar_mat


if __name__ == "__main__":
    current_path = os.getcwd()
    path = current_path + "/../../tests/assets/stochgrowth2_native.json"
    parser = NativeJSONParser(path)