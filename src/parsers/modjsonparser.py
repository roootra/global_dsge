import re
import json
import numpy
from numexpr import evaluate
from parsers.baseparser import *


class ModJSONParser(BaseParser):
    def __init__(self, path: str):
        super(ModJSONParser, self).__init__()
        with open(path, "r") as file:
            json_str = file.read().replace("^", "**")
            self.json_dict = json.loads(json_str)
        self.vars = self._parse_vars("endogenous")
        self.varexo = self._parse_vars("exogenous")
        self.params = self._parse_vars("parameters")
        self.dynamic_eqns = self._parse_eqns()
        self.static_eqns = self._parse_eqns(True)
        self.param_values = self._eval_param_values()
        self.ss_init_vals = self._eval_ss_init_vals()
        self.leads_lags = self._leads_lags()
        self.states_endo, self.controls = self._det_var_type()
        self.exo_covar_mat = self._parse_covar_mat()

    def _parse_vars(self, var_type: str) -> list:
        return [var["name"] for var in self.json_dict[var_type]]

    def _parse_eqns(self, static=False) -> list:
        lhs_rhs = self.json_dict["model"]
        eqns = list()
        for i in range(len(lhs_rhs)):
            eqns.append(lhs_rhs[i]["lhs"] + "-(" + lhs_rhs[i]["rhs"] + ")")
        if static:
            static_eqns = list()
            or_clause = "".join(var + "|" for var in self.varexo)[:-1]
            for eq in eqns:
                eq = re.sub(r"([a-zA-Z0-9]+)\(\+*-*\d+\)", r"\1", eq)
                eq = re.sub(rf"(\W|^)({or_clause})(\W|$)", r"\g<1>0\g<3>", eq)
                static_eqns.append(eq)
            eqns = static_eqns
        return eqns

    def _eval_param_values(self) -> dict:
        statements = self.json_dict["statements"]
        param_vals = dict()
        for statement in statements:
            if statement["statementName"] == "param_init":
                param_vals[statement["name"]] = float(evaluate(statement["value"]))
            if statement["statementName"] == "native":
                if "=" not in statement["string"]:
                    print(f"Matlab native code expression '{statement['string']}' does not look like an assignment, skipping...")
                else:
                    expression = statement["string"].split("=")
                    param_vals[expression[0].strip()] = float(evaluate(expression[1].replace(";", ""), param_vals))
        return param_vals

    def _eval_ss_init_vals(self) -> dict:
        statements = self.json_dict["statements"]
        init_vals = dict()
        for statement in statements:
            if statement["statementName"] == "initval":
                for val in statement["vals"]:
                    init_vals[val["name"]] = float(evaluate(val["value"], {**self.param_values, **init_vals}))
        return init_vals

    def _leads_lags(self) -> dict:
        leads_lags = dict()
        combine_eqns = ";".join([*self.dynamic_eqns]).replace(" ", "")
        for var in [*self.vars, *self.varexo]:
            leads_lags[var] = list(set(
                re.findall(f"{var}\(([\+\-0-9+]+)\)", combine_eqns)
            ))
            if len(set(re.findall(f"{var}(?!\(|[a-z])", combine_eqns))) > 0:
                leads_lags[var] = leads_lags[var] + [0]
            leads_lags[var] = list(map(int, leads_lags[var]))
        return leads_lags

    def _det_var_type(self):
        states = list()
        controls = list()
        for variable, lags in self.leads_lags.items():
            if variable in self.varexo:
                pass
            elif -1 in lags and 0 in lags:  # purely predetermined variables
                states.append(variable)
            elif -1 in lags and 0 in lags and 1 in lags:  # mixed variables
                states.append(variable)
            else:
                controls.append(variable)
        return states, controls

    def _parse_covar_mat(self):
        statements = self.json_dict["statements"]
        covar_mat = numpy.zeros((len(self.varexo),) * 2)
        for statement in statements:
            if statement["statementName"] == "shocks":
                if not statement["overwrite"]:
                    for variance in statement["variance"]:
                        var_ind = self.varexo.index(variance["name"])
                        covar_mat[var_ind, var_ind] = float(evaluate(variance["variance"], self.param_values))
                    for stderr in statement["stderr"]:
                        var_ind = self.varexo.index(stderr["name"])
                        covar_mat[var_ind, var_ind] = float(evaluate(stderr["stderr"], self.param_values)) ** 2
                    for covariance in statement["covariance"]:
                        var1_ind = self.varexo.index(covariance["name"])
                        var2_ind = self.varexo.index(covariance["name2"])
                        covar_mat[var1_ind, var2_ind] = float(evaluate(covariance["covariance"], self.param_values))
                        covar_mat[var2_ind, var1_ind] = covar_mat[var1_ind, var2_ind]
                    for correlation in statement["correlation"]:
                        var1_ind = self.varexo.index(correlation["name"])
                        var2_ind = self.varexo.index(correlation["name2"])
                        covar_mat[var1_ind, var2_ind] = float(evaluate(correlation["correlation"], self.param_values)) * \
                                                        covar_mat[var1_ind, var1_ind] * covar_mat[var2_ind, var2_ind]
                        covar_mat[var2_ind, var1_ind] = covar_mat[var1_ind, var2_ind]
        return covar_mat
