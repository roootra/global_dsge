from abc import ABC, abstractmethod
import numpy


class BaseParser(ABC):
    endo: list = NotImplemented
    shocks: list = NotImplemented
    param_values: dict = NotImplemented
    params: list = NotImplemented
    dynamic_eqns: list = NotImplemented
    static_eqns: list = NotImplemented
    ss_init_vals: dict = NotImplemented
    leads_lags: dict = NotImplemented
    states_endo: list = NotImplemented
    controls: list = NotImplemented
    exo_covar_mat: numpy.ndarray = NotImplemented
    vars: list = NotImplemented

    @abstractmethod
    def __init__(self):
        pass