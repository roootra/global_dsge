def domains_sstate(state_domains: dict, steady_state: dict):
    domains_formatted = {}
    for state in state_domains.keys():
        diff = state_domains[state]
        if state in steady_state.keys():
            domains_formatted[state] = [steady_state[state] - diff, steady_state[state] + diff]
        else:
            domains_formatted[state] = [-diff, diff]
    return domains_formatted

