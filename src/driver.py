# Copyright (C) 2021 Artur Zmanovskii
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# natives
import collections
import math
import random
import re
import os
from typing import Any, Optional
from operator import itemgetter
import copy
# third-parties
from numexpr import evaluate
import matplotlib
from matplotlib import pyplot as plt
from src.solvers.smolyaksolver import SmolyakSolver
from torch import no_grad
import numpy
# internal
import parsers
from src.steady.rootfinder import *

___VER___ = "0.1a"
___LEGALS___ = """Copyright (C) 2021 A. Zmanovskii
This program comes with ABSOLUTELY NO WARRANTY.
This is free software, and you are welcome to redistribute it
under certain conditions, described in GNU General Public License Version 3."""


class REDriver(object):
    """
    Instantiate a rational expectations model.

    :param file: str, path to a model declaration file.
    :param parser: Optional[BaseParser], a parser derived from BaseParser.
    """
    def __init__(self, file: str, parser: Optional[parsers.BaseParser] = None):
        print_greetings()
        self.parser, self._file_type = determine_parser(file)
        self.model = self.parser(file)
        self.sol = None
        self.steady_state = None
        print(self._file_type + " is loaded.")
        print(f"There are {len(self.model.dynamic_eqns)} equations and "
              f"{len(self.model.endo)} endogenous variables.")
        print("Control variables:")
        print(self.model.controls)
        print("Endogenous state variables:")
        print(self.model.states_endo)
        print("Exogenous state variables (shocks):")
        print(self.model.shocks)

    def steady(self, init_val: Optional[dict] = None, ret: bool = False):
        """
        Solve for steady state point.

        :param init_val: dict, initial values. Keys are endogenous variables, values are initial values.
        :param ret: bool, whether to return steady state point.
        :return: dict with steady state point, if required by parameter 'ret'. It is also available as attribute
        'steady_state'.
        """
        def eval_ss_loss(var_values: list):
            local_vars = dict((var, val) for var, val in zip(self.model.endo, var_values))
            locals_to_eval = {**self.model.param_values, **local_vars}
            return [evaluate(self.model.static_eqns[j], local_dict=locals_to_eval) for j
                    in range(len(self.model.static_eqns))]

        if init_val is not None:
            for var, val in init_val.items():
                self.model.ss_init_vals[var] = val
        uninit = set(self.model.endo).difference(set(self.model.ss_init_vals.keys()))
        if len(uninit) > 0: # if there are uninitialized vars, init them as U[0,1]
            for absent_var in uninit:
                self.model.ss_init_vals[absent_var] = random.uniform(0, 1)
        init_vals = itemgetter(*self.model.endo)(self.model.ss_init_vals)
        steady_state_vals = numpy.around(rootfinder(eval_ss_loss, init_vals), 16)
        self.steady_state = dict(zip(self.model.endo, steady_state_vals))
        if ret:
            return self.steady_state

    def solve(self, state_domains: dict, solver: Any = SmolyakSolver,
                    solver_kwargs: Optional[dict] = None, opt_method_kwargs: Optional[dict] = None,
                    verbose: bool = True) -> None:
        """
        Solve the stochastic model.

        :param state_domains: Optional[dict], keys are state variables, values are either floats --- ratios of
        relative deviation from steady state (ss * (1 - value), ss * (1 + value)), or lists with absolute bounds.
        :param solver: BaseSolver, approximation method.
        :param solver_kwargs: dict, arguments for solver.
        :param opt_method_kwargs: dict, arguments for optimization method.
        :param verbose: bool, whether to print equation solver results.
        :return: None.
        """
        if solver_kwargs is None:
            solver_kwargs = dict()
        if opt_method_kwargs is None:
            opt_method_kwargs = dict()
        if type(list(state_domains.values())[0]) is not list:
            if self.steady_state is None:
                raise RuntimeError("Steady state is not evaluated! Use .solve() method or specify lower and upper bounds"
                                   "for each state variable manually.")
            for var, ratio in state_domains.items():
                if var not in self.steady_state.keys():
                    state_domains[var] = [-ratio, ratio]
                else:
                    state_domains[var] = [self.steady_state[var]*(1-ratio), self.steady_state[var]*(1+ratio)]
            print("State variable domains are:")
            print(state_domains)
        self.solver = solver(model=self.model, state_domains=state_domains)
        results = self.solver.solve(verbose=verbose, opt_method_kwargs=opt_method_kwargs,  **solver_kwargs)
        #print(results["message"] + f" Success: {results['success']}.")
        #if not results["success"]:
        #    raise RuntimeError("Failed to solve model. Try again with different init_val.")
        self.sol = results

    def stoch_simul(self, shocks: dict, periods: int = 10, initial_state: Optional[dict] = None,
                    ortho: bool = True, graph: bool = True, moment_sims: int = 0) -> Any:
        """
        Simulate stochastic model conditional on a initial state and shocks at t=0.

        :param shocks: dict, keys are shock variables, values are shocks at t=0.
        :param periods: int, periods from t=0 to calculate.
        :param initial_state: Optional[dict], keys are variables' aliases, values are respective values. If None or
        not given, simulation starts from the steady state point.
        :param ortho: bool, whether to calculate orthogonalized shocks. Default = True.
        :param graph: bool, whether to visualize IRFs. Default = True.
        :return: dict, keys are variables' aliases, values are lists with variables' values from t=1.
        """
        if initial_state is None:
            initial_state = self.steady_state
        if not all(var in self.model.endo for var in initial_state):
            raise AssertionError("Not all endogenous variables' initial states are defined! "
                                 "If you don't want to specify controls, enter arbitrary value for them.")
        simul_path = self.solver.stoch_simul(initial_state=initial_state, shocks=shocks, periods=periods,
                                             ortho=ortho)
        empty_vars = {var: [] for var in self.model.endo + self.model.shocks}
        formatted_path = {shock: copy.deepcopy(empty_vars) for shock in shocks}
        with no_grad():
            for shock in formatted_path.keys():
                for t in range(periods):
                    for var in self.model.endo:
                        formatted_path[shock][var].append(simul_path[shock][t][var])
        if graph:
            #plt.ion()
            pages = math.ceil(len(self.model.vars) / 9)
            plt.rc('font', size=8)
            #matplotlib.use('TkAgg')
            for shock in formatted_path.keys():
                vars_iter = iter(self.model.vars)
                for page in range(pages):
                    fig, axes = plt.subplots(ncols=3, nrows=3)
                    for row in axes:
                        for col in row:
                            var = next(vars_iter, None)
                            if var in self.model.shocks:
                                continue
                            if var is None:
                                break
                            col.plot(formatted_path[shock][var], "r")
                            col.title.set_text(var)
                    plt.suptitle(f"Exogenous shock from {shock}.")
            plt.show(block=True)  # this plots all figures at once
        return simul_path


def determine_parser(file: str):
    extension = re.findall(r".([A-Za-z]+)$", file)
    if len(extension) == 0:
        raise ValueError("Cannot determine type of model file from extension! Please, specify parser manually.")
    if extension[-1] == "json":
        return parsers.ModJSONParser, "Dynare .json file"
    if extension[-1] == "yaml":
        return parsers.NativeYAMLParser, "Native .yaml file"
    else:
        raise ValueError("Unsupported model declaration file! Please, specify parser manually.")


def print_greetings():
    print("-" * 80)
    print(f"This is pyRatExp v{___VER___}.")
    print(___LEGALS___)
    print("-" * 80)


if __name__ == "__main__":
    from utils.statedomains import domains_sstate
    current_path = os.getcwd()
    path = current_path + "/tests/yaml/sg_2exos.yaml"
    path = current_path + "/tests/yaml/dynare_ex2.yaml"
    dynare_model = REDriver(file=path)
    ss = dynare_model.steady(ret=True)
    print("Steady state is:\n", ss)
    state_domains = {'c': 0.1, 'k': 0.1, 'A': 0.1, 'B': 0.1, 'eps_A': 0.1, 'eps_B': 0.1}
    state_domains = {'y': [-0.1, 0.1], 'c': [-0.1, 0.1], 'k': [1.5, 4], 'a': [-0.1, 0.1], 'h': [-3, 1],
                     'b': [-0.1, 0.1], 'e': [-0.05, 0.05], 'u': [-0.05, 0.05]}
    state_domains = {'y': 0.4, 'c': 0.4, 'k': 1.5, 'a': 0.1, 'h': 2,
                     'b': 0.1, 'e': 0.1, 'u': 0.1}
    state_domains = domains_sstate(state_domains, ss)
    dynare_model.solve(state_domains, solver_kwargs={'d_q': 1, 'multistep': True, 'solver_type': 'root',
                                                     'init_params': {'type': 'const',
                                                                             'domain': [-2, 2],
                                                                             'values': ss}},
                       opt_method_kwargs={'method': 'hybr'})
    shocks_path = {'eps_A': 0.01, 'eps_B': 0.01}
    shocks_path = {'e': 0.00, 'u': 0.00}
    dynare_model.stoch_simul(shocks=shocks_path, periods=100)
