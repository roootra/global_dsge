from unittest import TestCase
from src.parsers.modjsonparser import *
import os
import numpy


class TestModJSONParser(TestCase):
    def setUp(self):
        current_dir = os.getcwd()
        path = current_dir + "/assets/example1.json"
        self.jsonparser = ModJSONParser(path)

    def test__parse_vars(self):
        assert self.jsonparser.vars == ['y', 'c', 'k', 'a', 'h', 'b']

    def test__parse_eqns(self):
        test_eq = ['c*theta*h^(1+psi)=(1-alpha)*y',
                   'k=beta*c*exp(b)/(exp(b(1))*c(1))*(alpha*exp(b(1))*y(1)+k*(1-delta))',
                   'y=exp(a)*k(-1)^alpha*h^(1-alpha)',
                   'k=exp(b)*(y-c)+(1-delta)*k(-1)',
                   'a=rho*a(-1)+tau*b(-1)+e',
                   'b=a(-1)*tau+rho*b(-1)+u']
        assert self.jsonparser.dynamic_eqns == test_eq

    def test__eval_natives(self):
        assert self.jsonparser.natives["phi"] == 0.1

    def test__eval_param_values(self):
        test_params = dict(alpha=0.36, rho=0.95, tau=0.025, beta=0.99, delta=0.025, psi=0, theta=2.95)
        for var, val in self.jsonparser.param_values.items():
            assert test_params[var] == val

    def test__eval_ss_init_vals(self):
        test_init_vals = {'y': 1.08068253095672, 'c': 0.80359242014163, 'h': 0.29175631001732,
                          'k': 11.08360443260358, 'a': 0, 'b': 0,
                          'e': 0, 'u': 0}
        for var, val in self.jsonparser.ss_init_vals.items():
            assert test_init_vals[var] == val

    def test__leads_lags(self):
        test_leads_lags = {'y': [1, 0], 'c': [1, 0], 'k': [-1, 0], 'a': [-1, 0],
                           'h': [0], 'b': [1, -1, 0], 'e': [0], 'u': [0]}
        for var, val in self.jsonparser.leads_lags.items():
            assert set(val).issubset(test_leads_lags[var])
            assert set(test_leads_lags[var]).issubset(val)

    def test__det_var_type(self):
        test_states = ['k', 'a', 'b']
        test_controls = ['y', 'c', 'h']
        assert self.jsonparser.states_endo == test_states
        assert self.jsonparser.controls == test_controls

    def test__parse_covar_mat(self):
        test_mat = numpy.array([[0.009*0.009, 0.009*0.009*0.1], [0.009*0.009*0.1, 0.009*0.009]])
        numpy.testing.assert_array_equal(self.jsonparser.exo_covar_mat, test_mat)