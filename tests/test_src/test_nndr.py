import os
from unittest import TestCase
import torch
from src.projection.nndr import *
from src.driver import *


class TestNNDR(TestCase):
    def setUp(self) -> None:
        cwd = os.getcwd()
        self.driver = REDriver(cwd + "/../assets/stochgrowth.json")
        self.state_domains = dict(c=0.2, A=0.2, k=0.2, eps=0.1)

    def test_generate_points(self):
        nn_dr = NNDR(model=self.driver.model, state_domains=self.state_domains, layers=None, act_func=None)
        points_trunc = nn_dr.generate_points(1, True, distr="trunc_norm")
        torch.testing.assert_close(torch.tensor([-1.0, -1.0, -1.0], dtype=torch.float64), torch.squeeze(points_trunc))
        points_unif = nn_dr.generate_points(1, True, distr="uniform")
        points_nan = nn_dr.generate_points(1, True, distr=None)
        torch.testing.assert_close(points_unif, points_nan)

    def test_eval(self):
        nn_dr = NNDR(model=self.driver.model, state_domains=self.state_domains, layers=None, act_func=None)
        print(nn_dr.eval(torch.tensor([[0.1, 0.1, 0.1], [0.2, 0.2, 0.2]])))
