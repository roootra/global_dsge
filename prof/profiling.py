from src.model import GEModel
import os
import cProfile


def profile(func):
    def wrapper(*args, **kwargs):
        prof_filename = func.__name__ + '.prof'
        prof = cProfile.Profile()
        result = prof.runcall(func, *args, **kwargs)
        prof.dump_stats("" + prof_filename)
        return result
    return wrapper


@profile
def simple_sg_model(current_path: str=None):
    sg_model = GEModel(current_path + "/../tests/stochgrowth/stochgrowth_no_lambda.mod")
    state_domains = dict(k=0.5, A=0.5, eps=0.2)
    #initial_point = [1.50772743, 0.        , 0.        , 0.        , 0.        ,
    #                   0.        , 0.        , 5.83931164, 0.        , 0.        ,
    #                   0.        , 0.        , 0.        , 0.        , 1.        ,
    #                   0.        , 0.        , 0.        , 0.        , 0.        ,
    #                   0.        ]
    ss = sg_model.solve_steady_state(ret=True)
    print(ss)
    solver_kwargs = {"q": 4, "init_params": {"type": "const", "value": ss}}
    opt_method_kwargs = {"method": "hybr", "options": {"disp": True,
                                                       "ftol": 1e-10,
                                                       "maxiter": int(1e5),
                                                       "maxfev": 10000}}
    print()
    sg_model.solve(state_domains=state_domains, solver_kwargs=solver_kwargs,
                   opt_method_kwargs=opt_method_kwargs, verbose=True)
    sg_model.stoch_simul(shocks={"eps": 0.01}, periods=150, graph=False)


if __name__ == "__main__":
    current_path = os.getcwd()
    file = simple_sg_model(current_path)
    os.system("snakeviz " + "simple_sg_model.prof")