import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="TO BE FILLED",
    version="0.0.1",
    author="Artur Zmanovskii",
    author_email="anzmanovskii@gmail.com",
    description="Global solver for rational expectation models",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="TO BE FILLED",
    project_urls={
        "Bug Tracker": "TO BE FILLED",
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
        "Intended Audience :: Science/Research",
        "Topic :: Scientific/Engineering :: Mathematics",
    ],
    package_dir={"": "src"},
    packages=setuptools.find_packages(where="src"),
    python_requires=">=3.7",
)